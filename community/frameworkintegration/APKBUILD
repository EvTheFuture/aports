# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=frameworkintegration
pkgver=5.102.0
pkgrel=0
pkgdesc="Framework providing components to allow applications to integrate with a KDE Workspace"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org"
license="LGPL-2.1-or-later"
depends_dev="
	appstream-dev
	kconfig-dev
	kconfigwidgets-dev
	ki18n-dev
	kiconthemes-dev
	knewstuff-dev
	knotifications-dev
	kpackage-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	samurai
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/frameworkintegration-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2cbddc96e120371219ec626b491b68f0c1d7d738e65e9087829dee596e26a6263f5ff5f26d42e0de622f1940c32b92763a14f7f3aafa7bd8976897373287a06a  frameworkintegration-5.102.0.tar.xz
"
