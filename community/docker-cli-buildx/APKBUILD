# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=docker-cli-buildx
_commit=876462897612d36679153c3414f7689626251501
pkgver=0.10.0
pkgrel=0
pkgdesc="A Docker CLI plugin for extended build capabilities"
url="https://docs.docker.com/engine/reference/commandline/buildx_build"
arch="all"
license="Apache-2.0"
depends="docker-cli"
makedepends="go"
options="net"
source="buildx-$pkgver.tar.gz::https://github.com/docker/buildx/archive/v$pkgver.tar.gz"

_buildx_installdir="/usr/libexec/docker/cli-plugins"

builddir="$srcdir"/buildx-"$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	PKG=github.com/docker/buildx
	local ldflags="-X $PKG/version.Version=v$pkgver -X $PKG/version.Revision=$_commit -X $PKG/version.Package=$PKG"
	go build -modcacherw -ldflags "$ldflags" -o docker-buildx ./cmd/buildx
}

check() {
	# bake and gitutil tests do not succeed inside abuild environment
	local pkgs="$(go list -modcacherw ./... | grep -Ev '(bake|gitutil)')"
	go test -modcacherw -short $pkgs
	./docker-buildx version
}

package() {
	install -Dm755 docker-buildx "$pkgdir$_buildx_installdir"/docker-buildx
}

sha512sums="
94498e580989b94a7a1585302e37fa6253da7eb20f74eb92d464b3b32a7da5f6664e8479fdfa68597c1fc2794ee7807ff7950de283e60b0107ca632a17130d67  buildx-0.10.0.tar.gz
"
