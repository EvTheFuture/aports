# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-xarray
_pkgorig=xarray
pkgver=2023.1.0
pkgrel=0
pkgdesc="N-D labeled arrays and datasets in Python"
url="https://xarray.dev"
arch="noarch !s390x" # assertionErrors
license="Apache-2.0"
depends="python3 py3-numpy py3-packaging py3-pandas"
makedepends="python3-dev py3-setuptools_scm"
checkdepends="py3-coverage py3-mock py3-pytest py3-pytest-cov"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/x/xarray/xarray-$pkgver.tar.gz"
builddir="$srcdir/$_pkgorig-$pkgver"

build() {
	python3 setup.py build
}

check() {
	python3 -m pytest -k 'not test_dataset and not test_distributed and not test_dataarray'
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	rm -r "$pkgdir"/usr/lib/python3.*/site-packages/xarray/tests
}

sha512sums="
baddac6ed08816eb06b8153bc3443cc4ae77fc1d17d2cf242f543ce75b37e390f5c7e8c59cc292d1a586a78245cac614cf656a202f8d5ca81c176c4834533c4b  py3-xarray-2023.1.0.tar.gz
"
