# Contributor: Keith Maxwell <keith.maxwell@gmail.com>
# Maintainer: Roberto Oliveira <robertoguimaraes8@gmail.com>
pkgname=py3-google-api-python-client
_pkgname=google-api-python-client
pkgver=2.73.0
pkgrel=0
pkgdesc="Google API Client Library for Python"
url="https://github.com/googleapis/google-api-python-client"
arch="noarch !ppc64le"  # limited by py3-grpcio
license="Apache-2.0"
depends="
	py3-google-api-core
	py3-google-auth
	py3-google-auth-httplib2
	py3-httplib2
	py3-oauth2client
	py3-uritemplate
	"
makedepends="py3-setuptools"
checkdepends="
	py3-mock
	py3-openssl
	py3-parameterized
	py3-pytest
	"
source="https://files.pythonhosted.org/packages/source/g/google-api-python-client/google-api-python-client-$pkgver.tar.gz"

builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-google-api-python-client" # Backwards compatibility
provides="py-google-api-python-client=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build
}

check() {
	python3 -m pytest tests
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	# Fix permissions
	_site_packages=$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")
	chmod -R a+r "$pkgdir$_site_packages"
}

sha512sums="
7d280b26efec21ad9d520d5f059ff76ef7abdc13f5ccea4e9c17c32d0c35ac86758ebd557a08762b642b0c44d1a06b443a29a2572ed8ba880e1a00c74edfd80e  google-api-python-client-2.73.0.tar.gz
"
