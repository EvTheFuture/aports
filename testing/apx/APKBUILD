# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=apx
pkgver=1.4.2
pkgrel=0
pkgdesc="A package manager that can install packages from multiple sources without altering the root filesystem"
url="https://github.com/Vanilla-OS/apx"
license="GPL-3.0-only"
# s390x and riscv64 blocked by podman -> distrobox
arch="all !s390x !riscv64"
depends="distrobox"
makedepends="go"
subpackages="$pkgname-doc"
source="https://github.com/Vanilla-OS/apx/archive/$pkgver/apx-$pkgver.tar.gz
	config.json
	"
# net required to download go modules
# no tests
options="net !check"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -trimpath -v -o apx
}

package() {
	install -Dm755 apx "$pkgdir"/usr/bin/apx
	install -Dm644 "$srcdir"/config.json "$pkgdir"/etc/apx/config.json

	mkdir -p "$pkgdir"/usr/lib/apx
	ln -s /usr/bin/distrobox "$pkgdir"/usr/lib/apx/distrobox
	for cmd in create enter ephemeral 'export' generate-entry host-exec init list rm stop upgrade; do
		ln -s /usr/bin/distrobox-$cmd "$pkgdir"/usr/lib/apx/distrobox-$cmd
	done

	install -Dm755 man/apx.1 "$pkgdir"/usr/share/man/man1/apx.1
	install -Dm755 man/es/apx.1 "$pkgdir"/usr/share/man/es/man1/apx.1
}

sha512sums="
02546ff3c3abb92211f47963c7e9d1dea509c381fdce47accd8151bfa4db7c777ab8010603261c6acb377939d04c4a91d021e3a95de08ab87064c8b0dff4a28b  apx-1.4.2.tar.gz
c3f6c807bc4b4b0589fd552119142bddabf0d326a58d4524e6e7dba93a5f90058a2012ea822dc4e9822d7ef3235624a8fc0975a74723fde7bf4a8b00ec9850bd  config.json
"
